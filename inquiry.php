<?php
include('common/inquiry-header.php');
?>
<section class="inquiry-section">
	<div class="inquiry-form">
		<div class="inquery-heading">
			<h1>Inquiry</h1>
			<p>Please send your inquiry</p>
		</div>
		<form action="mail_handler.php" method="post" class="form-area">
			
			<div class="single-input">
				<label class="flex-basis" for="inquiry">Inquiry<span class="required">Required</span></label>
				
				<select name="inquiry" id="inquiry" tabindex="0" class="input-style flex-basis width-2x select-input" required>
					<option value=" " selected>Select Inquiry</option>
					<option value="About our services">About our services</option>
					<option value="Reqruitment">Reqruitment</option>
					<option value="Coverage">Coverage</option>
					<option value="Others">Others</option>
				</select>
			</div>
			<div class="single-input">
				<label class="flex-basis" for="label">Company<span class="required">Required</span></label>
				<input type="text" class="input-style flex-basis" name="company" id="company" value="" placeholder="" >
			</div>
			<div class="single-input">
				<label class="flex-basis" for="department">Department</label>
				<input type="text" class="input-style flex-basis" name="department" id="department" value="" placeholder="">
			</div>
			<div class="single-input">
				<label class="flex-basis" for="name">Name<span class="required">Required</span></label>
				<input type="text" class="input-style flex-basis width-2x" name="name" id="name" value="" placeholder="">
			</div>
			<div class="single-input">
				<label class="flex-basis" for="phonetic">Phonetic</label>
				<input type="text" class="input-style flex-basis width-2x" name="phonetic" id="phonetic" value="" placeholder="">
			</div>
			<div class="single-input">
				<label class="flex-basis" for="email">Email<span class="required">Required</span></label>
				<input type="email" class="input-style flex-basis" name="email" id="email" value="" placeholder="">
			</div>
			<div class="single-input">
				<label class="flex-basis" for="phone">Phone number</label>
				<input type="text" class="input-style flex-basis width-2x" name="phone" id="phone" value="" placeholder="">
			</div>
			<div class="single-input">
				<label class="flex-basis" for="address">Street address</label>
				<input type="text" class="input-style flex-basis" name="address" id="address" value="" placeholder="">
			</div>
			<div class="single-input">
				<label class="nathing" for="how_did_you">How did you know about us</label>
				<div class="all-checkbox">
					<label class="single-checkbox" for="newspaper">Newspaper
					  <input type="checkbox" value="" id="newspaper">
					  <span class="checkmark"></span>
					</label>
					<label class="single-checkbox" for="magazine">Magazine
					  <input type="checkbox" id="magazine" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="single-checkbox" for="search_engine">Search engine
					  <input type="checkbox" value="" id="search_engine">
					  <span class="checkmark"></span>
					</label>
					<label class="single-checkbox" for="other_website">Other website
					  <input type="checkbox" value="" id="other_website">
					  <span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="single-input">
				<label for="nocheckbox"></label>
				<input type="hidden" id="nocheckbox">
				<div class="all-checkbox second-all-checkbox">
					<label class="single-checkbox" for="ioa">Introduction of acquaintance
					  <input type="checkbox" value="" id="ioa">
					  <span class="checkmark"></span>
					</label>
					<label class="single-checkbox" for="mail_maganzine">Mail maganzine
					  <input type="checkbox" value="" id="mail_maganzine">
					  <span class="checkmark"></span>
					</label>
					<label class="single-checkbox" for="advertisement">Advertisement
					  <input type="checkbox" value="" id="advertisement">
					  <span class="checkmark"></span>
					</label>
					<label class="single-checkbox" for="others">Others
					  <input type="checkbox" value="" id="others">
					  <span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="single-input">
				<label class="flex-basis" for="content_of_inquiry">Content of inquiry</label>
				<textarea name="content_of_inquiry" id="content_of_inquiry"  class="input-style textarea-input flex-basis" cols="5" rows="3" accesskey="d" tabindex="0"></textarea>
			</div>
			<div class="single-input btn-input">
				<label for="send" class="flex-basis"></label>
				<button type="submit" name="submit" accesskey="c" tabindex="0"  title="send" id="send" value="" class="btn flex-basis custom-btn">Send</button>
			</div>
		</form>

	</div>
</section>
<?php
include('common/inquiry-footer.php');
?>