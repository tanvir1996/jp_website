<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>F1co | Inquiry</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/inquiry.css">
	<link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<!--  Header Section Start -->
<div id="top"></div>
<header id="navbar">
	
	<div class="main-menu">
		<nav>
			<div class="logo logo-inquiry">
				<img src="res/logo.png" alt="">
			</div>
			
			<div class="level-menu">
				<!-- <div class="header-top">
					<span><a href="http://womenindigital.net/projects/f1-mobile/inquiry.php" title="Japanese">Japanese </a></span>
					<span> | </span>
					<span><a href="#"> English </a></span>
				</div>
 -->

				<ul>
					<li><a href="index.html#top" title="Home">Home</a></li>
					<li><a href="index.html#business" title="Business">Business</a></li>
					<li><a href="index.html#recurit" title="Recurit">Recurit</a></li>
					<li><a href="index.html#about-us" title="About us">About us</a></li>
					<li><a href="inquiry.php" title="Inquiry">Inquiry</a></li>
				</ul>
			</div>
<div class="responsive-mebile-menu-icon">
				<a href="#" onclick="openNav()" class="inquery-menu">
  &#9776; 
</a> <!-- <img src="img/menubar.png" alt=""  /> -->
			</div>
		</nav>
	</div>
<div id="myNav" class="overlay">
	<div class="lang-menu">
	<span><a href="http://womenindigital.net/projects/f1-mobile/inquiry.php" title="Japanese ">Japanese </a></span>
					<span class="line"> | </span>
					<span><a href="#"> English </a></span>
	</div>
  <a href="#" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="overlay-content">
    <a href="index.html#top" title="Home">Home</a>
    <a href="index.html#business" title="Business">Business</a>
    <a href="index.html#recurit" title="Recurit">Recurit</a>
    <a href="index.html#about-us" title="About us">About us</a>
    <a href="inquiry.php" title="Inquiry">Inquiry</a>
  </div>
</div>
</header>

<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("navbar");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
<!--  Header Section End -->