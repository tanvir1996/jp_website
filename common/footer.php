<!--  Footer Start -->
<footer>
	<div class="footer-display-flex">
		<div class="footer-logo">
			<img src="img/footer_logo.png" alt="">
		</div>
		<div class="footer-level-menu">
			<ul>
				
				<li><a href="#top" title="Home">Home</a></li>
				<li><a href="#business" title="Business">Business</a></li>
				<li><a href="#recurit" title="Recurit">Recurit</a></li>
				<li><a href="#about-us" title="About us">About us</a></li>
				<li><a href="inquiry.php" title="Inquiry">Inquiry</a></li>
			</ul>
		 <p class="copy-right-text"> &#169; 2020 F1.CO.LTD</p>
		</div>
	</div>
</footer>
<!--  Footer end -->
<script src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
<script src="js/function.js"></script>
</body>
</html>