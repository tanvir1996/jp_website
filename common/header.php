<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Wid Project-459</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="css/ie.css" />
<![endif]-->
	
</head>
<body>
<!--  Header Section Start -->
<div id="top"></div>
<header id="navbar">
	
	<div class="main-menu">
		<nav>
			<div class="logo">
					<a href="./"><img src="img/logo.png" alt=""></a>
			</div>
			
			<div class="level-menu">
				<div class="header-top">
					<span><a href="http://womenindigital.net/projects/f1-mobile" title="Japanese">Japanese</a></span>
					<span> | </span>
					<span><a href="#"> English </a></span>
				</div>
				<ul>
					<li><a href="#top">Home</a></li>
					<li><a href="#business" title="business">Business</a></li>
					<li><a href="#recurit" title="recurit">Recurit</a></li>
					<li><a href="#about-us" title="about us">About us</a></li>
					<li><a href="inquiry.php" title="inquiry">Inquiry</a></li>
				</ul>
			</div>
<div class="responsive-mebile-menu-icon">
				<a href="#" onclick="openNav()">
  &#9776; 
</a> <!-- <img src="img/menubar.png" alt=""  /> -->
			</div>
		</nav>
	</div>
<!--  Responsive menu  -->
<div id="humbargurMenu">
<div id="myNav" class="overlay">
	<div class="lang-menu">
	<span><a href="http://womenindigital.net/projects/f1-mobile/" title="Japanese">Japanese</a></span>
					<span class="line"> | </span>
					<span><a href="#"> English </a></span>
	</div>
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="overlay-content">
       <a href="#top" id="hideHumbergurMenu">Home</a>
    <a href="#business"id="hideHumbergurMenu" title="Business">Business</a>
    <a href="#recurit" id="hideHumbergurMenu" title="Recurit">Recurit</a>
    <a href="#about-us" id="hideHumbergurMenu" title="About us">About us</a>
    <a href="inquiry.php" title="Inquiry">Inquiry</a>
 
  </div>
</div>
</header>
<!--  Header Section End -->